/*
 * title:			main.js
 * author:			Daniel K. Valente
 * created:			08-11-2023
 * modified:		08-11-2023
 * version:			0.0.0.0
 * license:			TBD
 * description:		Provides a starting point to load all of the JavaScript files accordingly.
 */

// JS files...
var GV_JSFileList={
	// {File Path} : {Function To Call In File}
	"services/client/js/donationPage.js":"donationPageLaunch"
};
// Calls the page initializer to setup the JS assets asynchronously (Avoids browser stalling/hanging).
setTimeout(function() {initializePage();},0);
// Initializes the page JS file assets.
function initializePage()
{
	let parent=document.querySelector("head");
	let item, value;
	for([item,value] of Object.entries(GV_JSFileList))
		loadAsset(parent, item, value);
}
// Returns a script HTML element object that can be appended to another HTML element.
function CreateScript(src)
{
	let elm=document.createElement("script");
	elm.src=src;
	elm.defer=true;
	return elm;
}
// Loads the JS asset file and appends it to the parent.
function loadAsset(parent, src, func)
{
	let elm=CreateScript(src); // Creates the JS script HTML element.
	if(func!==undefined && func!==null && typeof func === "string") // Checks if the function was specified.
	{
		// Calls the default/initialization function/method when the JS asset file completes loading.
		elm.onload=function(evtArgs)
		{
			if(window[func])
				window[func](evtArgs);
		};
	}
	parent.appendChild(elm); // Appends the JS asset file to the parent element.
}
