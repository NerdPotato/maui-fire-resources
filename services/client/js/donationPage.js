/*
 * title:			donationPage.js
 * author:			Daniel K. Valente
 * created:			08-11-2023
 * modified:		08-11-2023
 * version:			0.0.0.0
 * license:			TBD
 * description:		Provides a starting point to load all of the JavaScript files accordingly.
 */


// Initializes the donation page script assets.
function donationPageLaunch(args)
{
	console.log(args);
}

